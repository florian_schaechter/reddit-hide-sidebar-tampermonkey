// ==UserScript==
// @name         reddit-hide-sidebar
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  hides the sidebar
// @author       Florian Schaechter
// @match        https://old.reddit.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    //get header element
    var header = document.getElementById("header-bottom-right");

    //add seperatr
    var seperator = document.createElement("span");
    seperator.className = "seperator";
    seperator.innerHTML = "|";
    header.insertBefore(seperator,null);

    //add button
    var button = document.createElement("button");
    button.type = "button";
    button.innerHTML = "Hide Sidebar";
    button.id = "reddit-hide-sidebar-button";
    button.onclick = toggleSidebar;
    header.insertBefore(button,null);
})();

function toggleSidebar(){
  //console.log("button pressed");
  var btn = document.getElementById("reddit-hide-sidebar-button");
  var sidebar = document.getElementsByClassName("side")[0];
  //change button and hide/show sidebar
  if (btn.innerHTML === "Hide Sidebar"){
    sidebar.style.display = "none";
    btn.innerHTML = "Show Sidebar";
  }
  else {
    sidebar.style.display = "block";
    btn.innerHTML = "Hide Sidebar";
  }
  return;
};
